import { Entity, PrimaryGeneratedColumn, Column, BeforeInsert } from "typeorm"
import bcrypt from "bcryptjs"

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id?: number

  @Column({ unique: true })
  email?: string

  @Column({ select: false })
  password?: string

  @Column()
  full_name?: string

  @BeforeInsert()
  async hashPassword() {
    if (this.password != null) {
      this.password = await bcrypt.hash(this.password, Number(process.env.PASSWORD_SALT))
    }
  }
}
