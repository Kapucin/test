import { Router } from "express"
import { register, login, logout, me } from "../controllers/authController"
import authMiddleware from "../middlewares/authMiddleware"

const router: Router = Router()

router.post("/register", register)
router.post("/login", login)
router.post("/logout", logout)
router.get("/me", authMiddleware, me)

export default router
