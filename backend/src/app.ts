import "reflect-metadata"

import express, { Application } from "express"
import bodyParser from "body-parser"
import cors from "cors"
import authRoutes from "./routes/authRoutes"
import userRoutes from "./routes/userRoutes"
import { AppDataSource } from "./config/ormconfig"

const app: Application = express()

app.use(cors())

AppDataSource.initialize()
  .then(() => {
    console.log("Data Source has been initialized!")
  })
  .catch((err: any) => {
    console.error("Error during Data Source initialization:", err)
  })

app.use(bodyParser.json())

app.use("/api/auth", authRoutes)
app.use("/api/users", userRoutes)

app.listen(5001, () => {
  console.log("Server running on port 5001")
})

export default app
