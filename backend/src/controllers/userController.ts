import { Request, Response } from "express"
import { AppDataSource } from "../config/ormconfig"
import { User } from "../entities/User"

export const getUsers = async (req: Request, res: Response): Promise<void> => {
  const userRepository = AppDataSource.getRepository(User)
  const users = await userRepository.find()
  res.json(users)
}
