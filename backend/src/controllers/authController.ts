import { Request, Response } from "express"
import { AppDataSource } from "../config/ormconfig"
import { User } from "../entities/User"
import jwt from "jsonwebtoken"
import bcrypt, { hash } from "bcryptjs"
import { AuthRequest } from "../middlewares/authMiddleware"

const MIN_EMAIL_CHARACTERS = 8
const MIN_NAME_CHARACTERS = 5

export const register = async (req: Request, res: Response): Promise<void> => {
  const { email, password, fullName } = req.body
  if (!email) {
    res.status(400).json({ message: "Email must me not empty" })
    return
  }

  if (!password) {
    res.status(400).json({ message: "Password must me not empty" })
    return
  }

  if (!fullName || fullName.length < MIN_NAME_CHARACTERS) {
    res.status(400).json({
      message: `Full name must me not empty and more than ${MIN_NAME_CHARACTERS} characters`,
    })
    return
  }

  if (password.length < MIN_EMAIL_CHARACTERS) {
    res.status(400).json({
      message: `Password must me not empty and more than ${MIN_EMAIL_CHARACTERS} characters`,
    })
    return
  }

  const userRepository = AppDataSource.getRepository(User)

  try {
    const user = userRepository.create({ email, password: password, full_name: fullName })
    await userRepository.save(user)
    res.json({ token: createJwt(user) })
  } catch (error) {
    console.error(error)
    res.status(400).json({ message: "User already exists" })
  }
}

export const me = async (req: AuthRequest, res: Response): Promise<void> => {
  res.json({ fullName: req.user?.full_name })
}

export const login = async (req: Request, res: Response): Promise<void> => {
  const { email, password } = req.body

  const userRepository = AppDataSource.getRepository(User)
  const user = await userRepository
    .createQueryBuilder("user")
    .addSelect("user.password")
    .where("user.email = :email", { email })
    .getOne()

  if (!user) {
    res.status(401).json({ message: "Invalid credentials" })
    return
  }

  const isPasswordCorrect = await bcrypt.compare(password, user.password || "")
  if (!isPasswordCorrect) {
    res.status(401).json({ message: "Invalid credentials" })
    return
  }

  res.json({
    token: createJwt({ full_name: user.full_name, id: user.id } as User),
    fullName: user.full_name,
  })
}

export const createJwt = (user: User) => {
  return jwt.sign({ id: user.id, full_name: user.full_name }, process.env.JWT_SECRET as string, {
    expiresIn: "1h",
  })
}

export const logout = (req: Request, res: Response): void => {
  res.json({ message: "Logged out successfully" })
}
