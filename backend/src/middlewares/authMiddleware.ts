import { Request, Response, NextFunction } from "express"
import jwt from "jsonwebtoken"

interface AuthUser {
  email: string
  id: string
  full_name: string
}

export interface AuthRequest extends Request {
  user?: AuthUser
}

const AUTH_HEADER_NAME = "Authorization"
const AUTH_BEARER_NAME = "Bearer "

const authMiddleware = (req: AuthRequest, res: Response, next: NextFunction): void => {
  const token = req.header(AUTH_HEADER_NAME)?.replace(AUTH_BEARER_NAME, "")
  if (!token) {
    res.status(401).json({ message: "Authorization denied, no token" })
    return
  }

  try {
    req.user = jwt.verify(token, process.env.JWT_SECRET as string) as AuthUser
    next()
  } catch (err) {
    res.status(401).json({ message: "Authorization denied, token is not valid" })
  }
}

export default authMiddleware
