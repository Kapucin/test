export class Api {
  private static BASE_URL = "http://localhost:5001/api"

  public static baseUrl() {
    return Api.BASE_URL
  }

  public static me() {
    return `${Api.BASE_URL}/auth/me`
  }

  public static logout() {
    return `${Api.BASE_URL}/auth/logout`
  }

  public static login() {
    return `${Api.BASE_URL}/auth/login`
  }

  public static register() {
    return `${Api.BASE_URL}/auth/register`
  }

  public static users() {
    return `${Api.BASE_URL}/users`
  }
}
