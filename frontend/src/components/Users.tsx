import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Api } from '../utils/api';
import { message } from 'antd';
import { useNavigate } from 'react-router-dom';

const Users: React.FC = () => {
  const navigate = useNavigate();
  const [users, setUsers] = useState([]);
  const [messageApi, contextHolder] = message.useMessage();

  useEffect(() => {
    const fetchUsers = async () => {
      const token = localStorage.getItem('token');
      try {
        const response = await axios.get(Api.users(), {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        setUsers(response.data);
      } catch (e) {
        messageApi.error('Need to Login');
        navigate('/login');
      }
    };

    fetchUsers();
  }, []);

  return (
    <div>
      <h1>Users</h1>
      <ul>
        {users.map((user: any) => (
          <li key={user.id}>{user.full_name}</li>
        ))}
      </ul>
    </div>
  );
};

export default Users;

