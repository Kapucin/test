import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Api } from '../utils/api';

const Home: React.FC = () => {
  const token = localStorage.getItem('token');
  const [myName, setMyName] = useState('');
  const [isTokenValid, setIsTokenValid] = useState<boolean>(false);
  const [isChecking, setIsChecking] = useState<boolean>(true);

  useEffect(() => {
    (async() => {
      try {
        setIsChecking(true);
        const response = await axios.get(Api.me(), {
          headers: {
            'Authorization': 'Bearer ' + token
          }
        });
        setIsTokenValid(true);
        setMyName(response.data.fullName);
      } catch (error) {
        setIsTokenValid(false);
        console.error(error);
      } finally {
        setIsChecking(false);
      }
    })()
  }, []);

  const isHaveAuth = !isChecking && isTokenValid;
  const logoutHandler = async () => {
    try {
      await axios.post(Api.logout());
      setIsTokenValid(false);
      setMyName('');
      localStorage.removeItem('token');
    } catch (e) {
      console.error(e);
    }
  }
  return (
    <div>
      <div>Welcome {myName}</div>
      {isHaveAuth && <a onClick={logoutHandler}>To logout click here</a> }
    </div>
  );
};

export default Home;
