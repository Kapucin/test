import React from 'react';
import { Routes, Route, Link } from 'react-router-dom';
import Home from '../src/components/Home';
import Register from '../src/components/Register';
import Login from '../src/components/Login';
import Users from '../src/components/Users';
import { Layout, Menu, theme } from 'antd';

const { Header, Content, Footer } = Layout;

const items = [
  {
    key: 'home',
    label: <Link to="/">Home</Link>,
  },
  {
    key: 'register',
    label: <Link to="/register">Register</Link>,
  },
  {
    key: 'login',
    label: <Link to="/login">Login</Link>,
  },
  {
    key: 'users',
    label: <Link to="/users">Users</Link>,
  }
];

const App: React.FC = () => {
  const {
    token: { colorBgContainer, borderRadiusLG },
  } = theme.useToken();

  return (
    <Layout>
      <Header style={{ display: 'flex', alignItems: 'center' }}>
        <div className="demo-logo" />
        <Menu
          theme="dark"
          mode="horizontal"
          defaultSelectedKeys={['home']}
          items={items}
          style={{ flex: 1, minWidth: 0 }}
        />
      </Header>
      <Content style={{ padding: '0 48px' }}>
        <div
          style={{
            background: colorBgContainer,
            minHeight: 280,
            padding: 24,
            borderRadius: borderRadiusLG,
          }}
        >
          <div>
            <Routes>
              <Route path="/" element={<Home/>}/>
              <Route path="/register" element={<Register/>}/>
              <Route path="/login" element={<Login/>}/>
              <Route path="/users" element={<Users/>}/>
            </Routes>
          </div>
        </div>
      </Content>
      <Footer style={{textAlign: 'center'}}>
        Ant Design ©{new Date().getFullYear()} Created by Ivan Daka
      </Footer>
    </Layout>

  );
};

export default App;
