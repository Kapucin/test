# CRM Application

This project is a simple CRM (Customer Relationship Management) application for a brokerage company. The application allows users to register, log in, and log out using REST API calls. It also allows logged-in users to see a list of all users.

## Features

- User registration
- User login
- User logout
- View list of users
- Secure password storage with bcrypt
- JWT-based authentication

## Technologies Used

- Backend: Node.js, Express, TypeORM, PostgreSQL
- Frontend: React
- Authentication: JWT (JSON Web Tokens)
- Containerization: Docker
- Testing: Jest, Supertest
- Language: TypeScript

## Prerequisites

- Node.js (v20 or later)
- npm or yarn
- Docker

## Getting Started

### Backend
Install backend dependencies:
```bash
cd backend
npm install
```

### Frontend
Install backend dependencies:
```bash
cd frontend
npm install
```

### Running with Docker

1. Ensure Docker is installed and running on your machine.

2. Build and start the containers in root:
    ```bash
    npm run start
    ```

3. The backend will be available at `http://localhost:5001` and the frontend at `http://localhost:3000`.

## Testing

### Frontend Tests

To run the fronted tests, use:
```bash
cd frontend
npm test
```
